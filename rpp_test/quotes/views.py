from django.shortcuts import render
from rpp_test_tables.models import Quote, Contract
from django.views.generic import DetailView, TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy

class QuoteDetailView(DetailView):
    model = Quote

class QuoteCreateView(CreateView):
    model = Quote
    fields = ['service_level', 'service_level_desc', 'vendor', 'quantity', 'install_site_name',
              'address', 'begin_date', 'end_date', 'smartnet_service_sku', 'list_price', 'unit_price', 'extended_price']
    def form_valid(self, form, **kwargs):
        instance = form.save(commit=False)
        if not self.kwargs:
            pass
        else:
            con = Contract.objects.get(id=self.kwargs['pk'])
            instance.contract = con
        return super().form_valid(form)
    def get_context_data(self,**kwargs):
        context = super().get_context_data()
        if self.kwargs:
            context['contract'] = Contract.objects.get(id=self.kwargs['pk'])
        return context
    def get_success_url(self):
        return reverse('quote_list_view')

class QuoteUpdateView(UpdateView):
    model = Quote
    fields = ['contract', 'service_level', 'service_level_desc', 'vendor', 'date', 'part', 'quantity', 'install_site_name'
              'address', 'begin_date', 'end_date', 'smartnet_service_sku', 'list_price', 'unit_price', 'extended_price']
    def get_success_url(self):
        return reverse('quote_list_view')

class QuoteDeleteView(DeleteView):
    model = Quote
    def get_success_url(self):
        return reverse('quote_list_view')

class QuoteListView(ListView):
    model = Quote
