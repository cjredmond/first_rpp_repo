from django.shortcuts import render
from rpp_test_tables.models import PartNumber
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy

class PartNumberDetailView(DetailView):
    model = PartNumber

class PartNumberCreateView(CreateView):
    model = PartNumber
    fields = ['manufacturer', 'desc', 'num', 'end_of_life', 'end_of_service', 'end_of_support']
    def form_valid(self, form):
        instance = form.save(commit=False)
        return super().form_valid(form)
    def get_success_url(self):
        return reverse('part_list_view')

class PartNumberUpdateView(UpdateView):
    model = PartNumber
    fields = ['manufacturer', 'desc', 'num', 'end_of_life', 'end_of_service', 'end_of_support']
    def get_success_url(self):
        return reverse('part_list_view')

class PartNumberDeleteView(DeleteView):
    model = PartNumber
    def get_success_url(self):
        return reverse('part_list_view')
