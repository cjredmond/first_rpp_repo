from django.conf.urls import url, include
from django.contrib import admin
from quotes.views import QuoteDetailView, QuoteCreateView, QuoteUpdateView, QuoteDeleteView, QuoteListView
from parts.views import PartNumberDetailView, PartNumberCreateView, PartNumberUpdateView, PartNumberDeleteView
from retention_analyst.views import RetentionAnalystDetailView, RetentionAnalystCreateView, RetentionAnalystUpdateView, RetentionAnalystDeleteView
from sales.views import SalesAccountCreateView, SalesAccountDeleteView, SalesAccountUpdateView, SalesDetailView, SalesContractListView, SalesClientListView
from manufacturers.views import ManufacturerCreateView, ManufacturerUpdateView, ManufacturerDeleteView
from rpp_test_tables.views import passthru, ClientDetailView, ClientUpdateView, ClientCreateView, \
                                  AdminDashboard, ContractUpdateView, ProductUpdateView, ProductDetailView, ProductCreateView, \
                                  VendorCreateView, VendorUpdateView, VendorDeleteView, UserCreateView, ProductListView, ClientListView, \
                                  ContractListView, PartListView, ManufacturerListView, RetentionAnalystListView, SalesListView, VendorListView, \
                                  ContractDetailView, NewContractCreateView, ClientDetailContactInfoView, SalesContractCreateView, ContractNumberDetailView, ContractNumberListView, \
                                  NewClientDashboard
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import user_passes_test
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', auth_views.login, name='login'),
    url(r'^new-dashboard-demo/$', NewClientDashboard.as_view(), name='new_client_dashboard'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^passthru/$', passthru, name='passthru_view'),
    url(r'^admin-dashboard/$', user_passes_test(lambda u: u.is_superuser)(
AdminDashboard.as_view()), name='admin_dashboard'),
    url(r'^contract-number/(?P<pk>\d+)/$', ContractNumberDetailView.as_view(), name='contract_number_detail_view'),
    url(r'^contract-number/list/$', ContractNumberListView.as_view(), name='contract_number_list_view'),
    url(r'^client/(?P<pk>\d+)/contract/$', NewContractCreateView.as_view(), name='new_contract_create_view'),
    url(r'^client-detail/(?P<pk>\d+)/$', ClientDetailView.as_view(), name='client_detail_view'),
    url(r'^client/(?P<pk>\d+)/update/$', ClientUpdateView.as_view(), name='client_update_view'),
    url(r'^client-create/$', ClientCreateView.as_view(), name='client_create_view'),
    url(r'^client-list/$', ClientListView.as_view(), name='client_list_view'),
    url(r'^sales/(?P<pk>\d+)/clients/$', SalesClientListView.as_view(), name='sales_client_list_view'),
    url(r'^client-detail/(?P<pk>\d+)/contact-info/$', ClientDetailContactInfoView.as_view(), name='client_contact_view'),
    url(r'^sales/client/(?P<pk>\d+)/contract$', SalesContractCreateView.as_view(), name='sales_contract_create_view'),
    url(r'^contract/(?P<pk>\d+)/$', ContractDetailView.as_view(), name='contract_detail_view'),
    url(r'^contract/(?P<pk>\d+)/update/$', ContractUpdateView.as_view(), name='contract_update_view'),
    url(r'^contract-list/(?P<category>\w+)/$', ContractListView.as_view(), name='contract_list_view'),
    url(r'^product/(?P<pk>\d+)/detail/$', ProductDetailView.as_view(), name='product_detail_view'),
    url(r'^product/(?P<pk>\d+)/update/$', ProductUpdateView.as_view(), name='product_update_view'),
    url(r'^product-list/$', ProductListView.as_view(), name='product_list_view'),
    url(r'^client-detail/(?P<pk>\d+)/product/$', ProductCreateView.as_view(), name='product_create_view'),
    url(r'^retention-analyst/(?P<pk>\d+)/$', RetentionAnalystDetailView.as_view(), name='retention_analyst_detail'),
    url(r'^retention-analyst-create/$', RetentionAnalystCreateView.as_view(), name='retention_analyst_create_view'),
    url(r'^retention-analyst/(?P<pk>\d+)/edit$', RetentionAnalystUpdateView.as_view(), name='retention_analyst_update_view'),
    url(r'^retention-analyst/(?P<pk>\d+)/delete$', RetentionAnalystDeleteView.as_view(), name='retention_analyst_delete_view'),
    url(r'^retention-analyst-list/$', RetentionAnalystListView.as_view(), name='retention_analyst_list_view'),
    url(r'^vendor-create/$', VendorCreateView.as_view(), name='vendor_create_view'),
    url(r'^vendor/(?P<pk>\d+)/update/$', VendorUpdateView.as_view(), name='vendor_update_view'),
    url(r'^vendor/(?P<pk>\d+)/delete/$', VendorDeleteView.as_view(), name='vendor_delete_view'),
    url(r'^vendor-list/$', VendorListView.as_view(), name='vendor_list_view'),
    url(r'^manufacturer-create/$', ManufacturerCreateView.as_view(), name='manufacturer_create_view'),
    url(r'^manufacturer/(?P<pk>\d+)/update/$', ManufacturerUpdateView.as_view(), name='manufacturer_update_view'),
    url(r'^manufacturer/(?P<pk>\d+)/$', ManufacturerDeleteView.as_view(), name='manufacturer_delete_view'),
    url(r'^manufacturer-list/$', ManufacturerListView.as_view(), name='manufacturer_list_view'),
    url(r'^sales-agent-create/$', SalesAccountCreateView.as_view(), name='sales_account_create_view'),
    url(r'^sales/(?P<pk>\d+)/update/$', SalesAccountUpdateView.as_view(), name='salesaccount_update_view'),
    url(r'^sales/(?P<pk>\d+)/delete$', SalesAccountDeleteView.as_view(), name='salesaccount_delete_view'),
    url(r'^sales/(?P<pk>\d+)/$', SalesDetailView.as_view(), name='salesaccount_detail_view'),
    url(r'^sales-account-list/$', SalesListView.as_view(), name='salesaccount_list_view'),
    url(r'^sales/(?P<pk>\d+)/contracts/(?P<category>\w+)/$', SalesContractListView.as_view(), name='sales_contract_list_view'),
    url(r'^part-create/$', PartNumberCreateView.as_view(), name='part_create_view'),
    url(r'^part/(?P<pk>\d+)/edit/$', PartNumberUpdateView.as_view(), name='part_update_view'),
    url(r'^part/(?P<pk>\d+)/$', PartNumberDetailView.as_view(), name='part_detail_view'),
    url(r'^part/(?P<pk>\d+)/delete/$', PartNumberDeleteView.as_view(), name='part_delete_view'),
    url(r'^part-list/$', PartListView.as_view(), name='part_list_view'),
    url(r'^quote-create/(?P<pk>\d+)/$', QuoteCreateView.as_view(), name='quote_create_view'),
    url(r'^quote-create/$', QuoteCreateView.as_view(), name='quote_create_view_new'),
    url(r'^quote/(?P<pk>\d+)/edit/$', QuoteUpdateView.as_view(), name='quote_update_view'),
    url(r'^quote/(?P<pk>\d+)/delete/$', QuoteDeleteView.as_view(), name='quote_delete_view'),
    url(r'^quote/(?P<pk>\d+)/$', QuoteDetailView.as_view(), name='quote_detail_view'),
    url(r'^quote-list/$', QuoteListView.as_view(), name='quote_list_view'),
    url(r'^rentention-analyst/(?P<reta>\d+)/login/(?P<pk>\d+)/$', UserCreateView.as_view(), name='user_create_view_reta'),
    url(r'^client/(?P<client>\d+)/login/(?P<pk>\d+)/$', UserCreateView.as_view(), name='user_create_view_client'),
    url(r'^sales/(?P<agent>\d+)/login/(?P<pk>\d+)/$', UserCreateView.as_view(), name='user_create_view_sales'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
