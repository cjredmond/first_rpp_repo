from django.shortcuts import render
from django.views.generic import DetailView, TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rpp_test_tables.models import Product, SalesAccount, Client, Contract, ClientLogin, SalesAccountLogin, Vendor, RetentionAnalyst, Manufacturer, PartNumber, AdminLogin, Quote, ClientUser, ClientUpdate, ContractUpdate, ContractUpdateEmailLog, ContractNumber
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from rpp_test_tables.forms import ProductForm, ContractForm, ManufacturerForm, PartForm, VendorForm, SalesForm
from datetime import datetime, timedelta, timezone
from django.utils import timezone
from django.contrib.auth.models import Group, User
from django.contrib.auth.forms import UserCreationForm
from rpp_test_tables.messages import get_new_clients, get_trueups, get_ending_contracts, new_client_message_list, get_new_contracts
from django.core.mail import send_mail, EmailMultiAlternatives
import os
import sendgrid
from sendgrid.helpers.mail import *
from rpp_test_tables.email_scripts import *

if 1 == 8:
    con = ContractNumber.objects.all().first()
    contract_report_email(con)

class NewClientDashboard(TemplateView):
    template_name = 'new_client_dashboard.html'

class AdminDashboard(TemplateView):
    template_name = 'partials/new_dash/four_boxes.html'
    def get_context_data(self):
        context = super().get_context_data()
        context['clients'] = Client.objects.all()
        context['contracts'] = Contract.objects.all().order_by('vendor')
        context['products'] = Product.objects.all()
        context['vendors'] = Vendor.objects.all()
        context['retention_analysts'] = RetentionAnalyst.objects.all()
        context['sales'] = SalesAccount.objects.all()
        context['manufacturers'] = Manufacturer.objects.all()
        context['parts'] = PartNumber.objects.all()
        context['sales_today'] = Contract.objects.filter(start_date=timezone.now().date()).count()
        context['user_form'] = UserCreationForm
        context['manufacturer_form'] = ManufacturerForm
        context['admin_logins'] = AdminLogin.objects.filter(admin=self.request.user)
        context['new_clients'] = get_new_clients().count()
        context['trueups'] = get_trueups()
        context['groups'] = Group.objects.all()
        context['expiring_contracts'] = get_ending_contracts()
        context['expired_cons'] = [x for x in Contract.objects.all() if x.end_date < timezone.now().date()]
        context['expired_life'] = [x for x in Contract.objects.all() if x.end_of_life() < timezone.now().date() ]
        context['expiring_life'] = [x for x in Contract.objects.all() if x.end_of_life() < timezone.now().date() + timedelta(days=180)]
        context['expiring_service'] = [x for x in Contract.objects.all() if x.end_of_service() < timezone.now().date() + timedelta(days=180)]
        context['expired_service'] = [x for x in Contract.objects.all() if x.end_of_service() <= timezone.now().date()]
        context['quotes'] = Quote.objects.exclude(contract=None)
        context['new_client_list'] = new_client_message_list()
        context['new_contract_list'] = get_new_contracts()
        context['message_amount'] = get_new_clients().count() + len(get_new_contracts())
        return context

class ProductDetailView(DetailView):
    model = Product

class ProductCreateView(CreateView):
    model = Product
    fields = ['name', 'part_num', 'serial', 'purchase_date', 'disposal', 'service_category', 'price',
              'quantity_min', 'quantity_max', 'duration', 'orderability', 'item_identifier', 'service_program',
              'category_base_discount_name', 'end_of_sale_date']
    def form_valid(self,form,**kwargs):
        instance = form.save(commit=False)
        client = Client.objects.get(id=self.kwargs['pk'])
        instance.client = client
        return super().form_valid(form)
    def get_success_url(self,**kwargs):
        target = self.kwargs['pk']
        return reverse_lazy('client_detail_view', args=(str(target),))

class SalesContractCreateView(CreateView):
    model = Contract
    fields = ['serial', 'start_date', 'end_date', 'cost', 'vendor', 'commission',
              'service_level', 'site_id', 'quantity', 'pak_num', 'smart_net_sku', 'smark_net_desc',
              'annual_list_price', 'trade_value', 'product']
    def get_context_data(self,**kwargs):
        context = super().get_context_data()
        context['form'].fields['product'].queryset=Product.objects.filter(client_id=self.kwargs['pk'])
        return context
    def form_valid(self, form, **kwargs):
        instance = form.save(commit=False)
        client = Client.objects.get(id=self.kwargs['pk'])
        agent = self.request.user.salesuser.sales_account
        instance.client = client
        instance.sale_agent = agent
        instance.address = client.address
        instance.city = client.city
        instance.state = client.state
        instance.zip_code = client.zip_code
        instance.country = client.country
        instance.site_name = client.name
        return super().form_valid(form)
    def get_success_url(self,**kwargs):
        target = self.kwargs['pk']
        return reverse('client_detail_view', args=(str(target),))

class NewContractCreateView(CreateView):
    model = Contract
    fields = ['serial', 'start_date', 'end_date', 'cost', 'vendor', 'sale_agent', 'commission',
              'service_level', 'site_id', 'quantity', 'pak_num', 'smart_net_sku', 'smark_net_desc',
              'annual_list_price', 'trade_value', 'product']
    def get_context_data(self,**kwargs):
        context = super().get_context_data()
        context['form'].fields['product'].queryset=Product.objects.filter(client_id=self.kwargs['pk'])
        return context
    def form_valid(self,form):
        instance = form.save(commit=False)
        client = Client.objects.get(id=self.kwargs['pk'])
        instance.address = client.address
        instance.city = client.city
        instance.state = client.state
        instance.zip_code = client.zip_code
        instance.country = client.country
        instance.site_name = client.name
        return super().form_valid(form)
    def get_success_url(self,**kwargs):
        target = self.kwargs['pk']
        return reverse('client_detail_view', args=(str(target),))

class ContractUpdateView(UpdateView):
    model = Contract
    fields = ['serial', 'start_date', 'end_date', 'cost', 'vendor', 'sale_agent', 'commission', 'service_level', 'site_id',
              'site_name', 'address', 'city', 'state', 'zip_code', 'country', 'quantity', 'pak_num', 'smart_net_sku', 'smark_net_desc',
              'annual_list_price', 'trade_value']
    def get_success_url(self, **kwargs):
        target = self.kwargs['pk']
        con = Contract.objects.get(id=target)
        contract_update_email(con,con.client.correspondence_signature)
        ContractUpdate.objects.create(user=self.request.user, contract=Contract.objects.get(id=target))
        return reverse_lazy('contract_detail_view', args=(str(target),))

class ContractDetailView(DetailView):
    model = Contract

class ContractNumberDetailView(DetailView):
    model = ContractNumber

class ContractNumberListView(ListView):
    model = ContractNumber

class ProductUpdateView(UpdateView):
    model = Product
    fields = ['part_num', 'serial', 'disposal', 'client', 'purchase_date']
    def get_success_url(self):
        if self.request.user.is_superuser:
            return reverse('product_list_view')
        return reverse('admin_dashboard')

@method_decorator(login_required, name='dispatch')
class ClientDetailView(DetailView):
    model = Client
    def user_passes_test(self,request,**kwargs):
        if request.user.is_superuser:
            return True
        elif SalesUser.objects.filter(user=self.request.user):
            if request.user.salesuser.sales_account in Client.objects.get(id=self.kwargs['pk']).active_sales_agents:
                return True
        elif request.user.clientuser.client == Client.objects.get(id=self.kwargs['pk']):
            return True
        return False

    def dispatch(self,request,*args,**kwargs):
        if not self.user_passes_test(request):
            return HttpResponseRedirect(reverse('login'))
        return super(ClientDetailView, self).dispatch(request,*args,**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        client = Client.objects.get(id=self.kwargs['pk'])
        context['orphan_products'] = [x for x in Product.objects.filter(client=client) if not x.contract_set.all()]
        return context

class ClientDetailContactInfoView(TemplateView):
    template_name = "partials/new_dash/client_contact.html"
    def get_context_data(self,**kwargs):
        context = super().get_context_data()
        client = Client.objects.get(id=self.kwargs['pk'])
        context['agents'] = client.active_sales_agents
        context['analysts'] = client.active_analysts
        return context

@login_required
def passthru(request):
    if SalesAccount.objects.filter(user=request.user):
        sales_account = SalesAccount.objects.get(user=request.user)
        SalesAccountLogin.objects.create(agent=sales_account)
        return HttpResponseRedirect(reverse('salesaccount_detail_view', args=[request.user.id]))
    elif request.user.is_superuser:
        AdminLogin.objects.create(admin=request.user)
        return HttpResponseRedirect(reverse('admin_dashboard'))
    elif RetentionAnalystUser.objects.filter(user=request.user):
        return HttpResponseRedirect(reverse('retention_analyst_detail', args=[request.user.retentionanalystuser.reta.id]))
    elif ClientUser.objects.filter(user=request.user):
        target = ClientUser.objects.get(user=request.user)
        ClientLogin.objects.create(client=target.client)
        return HttpResponseRedirect(reverse('client_detail_view', args=[request.user.clientuser.client.id]))
    else:
        return HttpResponseRedirect(reverse('login'))

class VendorCreateView(CreateView):
    model = Vendor
    def get_success_url(self):
        return reverse('vendor_list_view')
    fields = ['name', 'lead_product', 'address', 'city', 'state', 'zip_code', 'website', 'phone', 'status']
    def form_valid(self, form):
        instance = form.save(commit=False)
        return super().form_valid(form)

class VendorUpdateView(UpdateView):
    model = Vendor
    fields = ['name', 'address', 'website', 'phone', 'status']
    def get_success_url(self):
        return reverse('vendor_list_view')

class VendorDeleteView(DeleteView):
    model = Vendor
    def get_success_url(self):
        return reverse('vendor_list_view')

class UserCreateView(CreateView):
    model = User
    form_class = UserCreationForm
    def form_valid(self, form, **kwargs):
        instance = form.save(commit=False)
        x = self.request.POST['cell']
        if form.is_valid():
            instance.save()
            group = Group.objects.get(id=self.kwargs['pk'])
            instance.groups.add(group)
            if self.kwargs['pk'] == '1':
                sales = SalesAccount.objects.get(id=self.kwargs['agent'])
                new = SalesUser.objects.create(user=instance,sales_account=sales,cell=x)
                new.save()
                return HttpResponseRedirect(reverse('`salesacc`ount_list_view'))
            elif self.kwargs['pk'] == '2':
                client = Client.objects.get(id=self.kwargs['client'])
                new = ClientUser.objects.create(user=instance,client=client,cell=x)
                new.save()
                return HttpResponseRedirect(reverse('client_list_view'))
            elif self.kwargs['pk'] == '3':
                reta = RetentionAnalyst.objects.get(id=self.kwargs['reta'])
                new = RetentionAnalystUser.objects.create(user=instance,reta=reta,cell=x)
                new.save()
                return HttpResponseRedirect(reverse('retention_analyst_detail', args=[self.kwargs['reta']]))


        return super().form_valid(form)
    def get_success_url(self, **kwargs):
        if self.kwargs['pk'] == 1:
            return reverse('sales_account_list_view')
        elif self.kwargs['pk'] == 2:
            return reverse('client_list_view')
        elif self.kwargs['pk'] == 3:
            return reverse('retention_analyst_list_view')

class ClientCreateView(CreateView):
    model = Client
    fields = ['name', 'cell_num', 'lead_product', 'phone', 'status', 'address', 'city', 'state', 'zip_code',
              'website', 'web_admin', 'web_admin_cell', 'correspondence_signature', 'correspondence_signature_title',
              'correspondence_phone', 'correspondence_contact_one', 'correspondence_contact_two', 'correspondence_email', 'copy_emails_to_portal_list',
              'list_discount', 'list_discount_international', 'service_discount', 'override_sales_tax', 'discount_off_list_percent', 'rate_factor_year',
              'rate_factor_two_year', 'rate_factor_three_year', 'rate_factor_increase', 'residual_value', 'aquisition_fee', 'email_address_salesperson']
    def get_success_url(self):
        if self.request.user.is_superuser:
            return reverse('client_list_view')
        return reverse('sales_client_list_view', args=[self.request.user.salesuser.sales_account.id])
    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.added_by = self.request.user
        return super().form_valid(form)


class ClientUpdateView(UpdateView):
    model = Client
    fields = ['name', 'cell_num', 'lead_product', 'phone', 'status', 'address', 'city', 'state', 'zip_code',
              'website', 'web_admin', 'web_admin_cell', 'correspondence_signature', 'correspondence_signature_title',
              'correspondence_phone', 'correspondence_contact_one', 'correspondence_contact_two', 'correspondence_email', 'copy_emails_to_portal_list',
              'list_discount', 'list_discount_international', 'service_discount', 'override_sales_tax', 'discount_off_list_percent', 'rate_factor_year',
              'rate_factor_two_year', 'rate_factor_three_year', 'rate_factor_increase', 'residual_value', 'aquisition_fee', 'email_address_salesperson']
    def get_success_url(self, **kwargs):
        ClientUpdate.objects.create(user=self.request.user, client=Client.objects.get(id=self.kwargs['pk']))
        return reverse('client_detail_view', args=[self.kwargs['pk']])

class ProductListView(ListView):
    model = Product
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

class ClientListView(ListView):
    model = Client

class ContractListView(ListView):
    model = Contract
    def get_queryset(self, **kwargs):
        if self.request.user.is_superuser:
            if self.kwargs['category'] == 'number':
                return Contract.objects.order_by('serial')
            elif self.kwargs['category'] == 'client':
                return Contract.objects.order_by('client')
            elif self.kwargs['category'] == 'vendor':
                return Contract.objects.order_by('vendor')
            elif self.kwargs['category'] == 'agent':
                return Contract.objects.order_by('sale_agent')
            elif self.kwargs['category'] == 'analyst':
                return Contract.objects.order_by('sale_agent')
            elif self.kwargs['category'] == 'end_date':
                return Contract.objects.order_by('end_date')
            return Contract.objects.all()

class PartListView(ListView):
    model = PartNumber
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

class ManufacturerListView(ListView):
    model = Manufacturer
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

class RetentionAnalystListView(ListView):
    model = RetentionAnalyst
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

class SalesListView(ListView):
    model = SalesAccount
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

class VendorListView(ListView):
    model = Vendor
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context
