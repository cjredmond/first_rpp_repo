from rpp_test_tables.models import Client, Contract, Product, ContractUpdateEmailLog, ContractReportEmailLog
from rpp_test_tables.excel_scripts import contract_layout
from django.core.mail import send_mail, EmailMultiAlternatives
import os
import sendgrid
from sendgrid.helpers.mail import *
from rpp_test_tables.email_scripts import *
import urllib
import datetime
import xlsxwriter


def contract_report_email(contract_num):
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SG_API_KEY'))
    from_email = Email('admin@rpp.com')
    to_email = Email('tquinn@pedestaltechnology.com')
    subject = 'Test Django/Sendgrid'
    content = Content('text/html', 'Test Djano/Sendgrid email')
    mail = Mail(from_email, subject, to_email, content)
    # mail.personalizations[0].add_substitution(Substitution("%client%", recipient))
    # mail.personalizations[0].add_substitution(Substitution("%contract_number%", contract.serial ))
    # mail.template_id = "51bac4ab-0950-4761-879e-db327761159c"
    mail.personalizations[0].add_substitution(Substitution("%Contract_Number%", contract_num.number ))
    mail.personalizations[0].add_substitution(Substitution("%Correspondence_Name%", 'testClient'))
    mail.personalizations[0].add_substitution(Substitution("%Contract_End_Date%", 'Tuesday, October 17th'))
    mail.personalizations[0].add_substitution(Substitution("%Salesperson_Email%", 'sales@rpp.com'))
    mail.template_id = '82292f17-c162-4113-9280-6339181a76f4'
    try:
        response = sg.client.mail.send.post(request_body=mail.get())
    except urllib.error.HTTPError as e:
        print(e.read())
        exit()
    email_content = 'Hello, {}. Your contract {} has been updated.'.format(contract_num.client.name, contract_num.number)
