from django import forms
from django.contrib.auth.forms import UserCreationForm
from rpp_test_tables.models import Product, SalesAccount, Contract, Manufacturer, PartNumber, Vendor

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['part_num', 'serial', 'purchase_date', 'disposal']
        labels = { 'part_num': 'Part Number',
                   'serial': 'Serial Number',
                   'purchase_date': 'Purchase Date',
                   'disposal': 'Disposal (optional)'}

class ContractForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = ['serial', 'start_date', 'end_date', 'cost', 'vendor', 'sale_agent', 'commission', 'service_level', 'site_id',
                  'site_name', 'address', 'city', 'state', 'zip_code', 'country', 'quantity', 'pak_num', 'smart_net_sku', 'smark_net_desc',
                  'annual_list_price', 'trade_value', 'product']
        labels = { 'serial': 'Serial Number',
                   'start_date': 'Start Date',
                   'end_date': 'End Date',
                   'cost': 'Cost',
                   'vendor': 'Vendor',
                   'sale_agent': 'Sales Agent',
                   'commission': 'Commission (optional)'}

class ManufacturerForm(forms.ModelForm):
    class Meta:
        model = Manufacturer
        fields = ['name', 'address', 'website', 'status', 'phone', 'logo']

class PartForm(forms.ModelForm):
    class Meta:
        model = PartNumber
        fields = ['manufacturer', 'desc', 'end_of_life', 'end_of_service', 'end_of_support']

class VendorForm(forms.ModelForm):
    class Meta:
        model = Vendor
        fields = ['name', 'lead_product', 'address', 'city', 'state', 'zip_code', 'website', 'phone', 'status']

class SalesForm(forms.ModelForm):
    class Meta:
        model = SalesAccount
        fields = ['name', 'vendors', 'retention_analyst']
