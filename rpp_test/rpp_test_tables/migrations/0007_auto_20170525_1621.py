# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-25 16:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rpp_test_tables', '0006_clientlogin_salesaccountlogin'),
    ]

    operations = [
        migrations.AddField(
            model_name='partnumber',
            name='end_of_life',
            field=models.DateField(default='2022-1-1'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='partnumber',
            name='end_of_service',
            field=models.DateField(default='2020-1-1'),
            preserve_default=False,
        ),
    ]
