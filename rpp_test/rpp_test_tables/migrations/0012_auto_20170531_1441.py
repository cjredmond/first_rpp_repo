# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-31 14:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rpp_test_tables', '0011_auto_20170531_1433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesaccount',
            name='name',
            field=models.CharField(blank=True, max_length=144, null=True),
        ),
        migrations.AlterField(
            model_name='salesaccount',
            name='retention_analyst',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='rpp_test_tables.RetentionAnalyst'),
        ),
        migrations.AlterField(
            model_name='salesaccount',
            name='vendors',
            field=models.ManyToManyField(to='rpp_test_tables.Vendor'),
        ),
    ]
