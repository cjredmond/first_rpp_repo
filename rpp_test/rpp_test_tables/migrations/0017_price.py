# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-02 14:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rpp_test_tables', '0016_auto_20170602_1445'),
    ]

    operations = [
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.FloatField()),
                ('date', models.DateField(auto_now_add=True)),
                ('part', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rpp_test_tables.PartNumber')),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rpp_test_tables.Vendor')),
            ],
        ),
    ]
