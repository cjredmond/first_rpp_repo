# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-27 18:14
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('rpp_test_tables', '0042_clientupdate'),
    ]

    operations = [
        migrations.CreateModel(
            name='SalesUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cell', models.CharField(max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='salesaccount',
            name='cell_num',
        ),
        migrations.RemoveField(
            model_name='salesaccount',
            name='user',
        ),
        migrations.AddField(
            model_name='salesuser',
            name='sales_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rpp_test_tables.SalesAccount'),
        ),
        migrations.AddField(
            model_name='salesuser',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
