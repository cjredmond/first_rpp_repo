# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-25 14:26
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('rpp_test_tables', '0061_contractreportemaillog_completed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='retentionanalystuser',
            name='reta',
        ),
        migrations.RemoveField(
            model_name='retentionanalystuser',
            name='user',
        ),
        migrations.RemoveField(
            model_name='salesuser',
            name='sales_account',
        ),
        migrations.RemoveField(
            model_name='salesuser',
            name='user',
        ),
        migrations.AddField(
            model_name='retentionanalyst',
            name='user',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='salesaccount',
            name='user',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='RetentionAnalystUser',
        ),
        migrations.DeleteModel(
            name='SalesUser',
        ),
    ]
