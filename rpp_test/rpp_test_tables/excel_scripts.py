from rpp_test_tables.models import Client, Contract, Product, ContractUpdateEmailLog, ContractReportEmailLog
import datetime
import xlsxwriter

def contract_layout(contract_num):
    contracts = contract_num.contract_set.all()
    workbook = xlsxwriter.Workbook('excel/{}-{}-{}.xlsx'.format(contract_num.client.name, contract_num.number ,datetime.date.today().strftime('%m-%d-%y')))
    bold = workbook.add_format({'bold': True})
    red = workbook.add_format({'font_color': 'red'})

    worksheet = workbook.add_worksheet()
    worksheet.write('A1','Contract #',bold)
    worksheet.write('B1','Service Level',bold)
    worksheet.write('C1','Site ID',bold)
    worksheet.write('D1','Site Name',bold)
    worksheet.write('E1','Address',bold)
    worksheet.write('F1','City',bold)
    worksheet.write('G1','State',bold)
    worksheet.write('H1','Zip',bold)
    worksheet.write('I1','Country',bold)
    worksheet.write('J1','Quantity',bold)
    worksheet.write('K1','Product Number',bold)
    worksheet.write('L1','Serial/PAK #',bold)
    worksheet.write('M1','Description',bold)
    worksheet.write('N1','Begin Date',bold)
    worksheet.write('O1','End Date',bold)
    worksheet.write('P1','End of Life',bold)
    worksheet.write('Q1','End of Service',bold)
    worksheet.write('R1','End of Support',bold)

    row = 1
    col = 0
    for con in contracts:
        worksheet.write(row,col,con.serial)
        worksheet.write(row,col+1,con.service_level)
        worksheet.write(row,col+2,con.client.id)
        worksheet.write(row,col+3,con.client.name)
        worksheet.write(row,col+4,con.client.address)
        worksheet.write(row,col+5,con.client.city)
        worksheet.write(row,col+6,con.client.state)
        worksheet.write(row,col+7,con.client.zip_code)
        worksheet.write(row,col+8,con.client.country)
        worksheet.write(row,col+9,con.quantity)
        worksheet.write(row,col+10,con.product.serial)
        worksheet.write(row,col+11,con.pak_num)
        worksheet.write(row,col+12,con.product.part_num.desc)
        worksheet.write(row,col+13,con.start_date.strftime('%x'))
        if con.ninety:
            worksheet.write(row,col+14,con.end_date.strftime('%x'),red)
        else:
            worksheet.write(row,col+14,con.end_date.strftime('%x'))
        worksheet.write(row,col+15,con.product.part_num.end_of_life.strftime('%x'))
        worksheet.write(row,col+16,con.product.part_num.end_of_service.strftime('%x'))
        worksheet.write(row,col+17,con.product.part_num.end_of_support.strftime('%x'))
        row += 1
    workbook.close()

    
