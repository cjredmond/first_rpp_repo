from django.apps import AppConfig


class RppTestTablesConfig(AppConfig):
    name = 'rpp_test_tables'
