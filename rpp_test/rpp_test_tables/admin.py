from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from rpp_test_tables.models import Client, Product, PartNumber, Contract, Vendor,\
 Manufacturer, SalesAccount, ClientLogin, SalesAccountLogin, RetentionAnalyst, Quote, \
 ClientUser, ClientUpdate,  ContractUpdateEmailLog, ContractNumber

admin.site.register([Product, PartNumber, Contract, Vendor, \
                     Manufacturer, SalesAccount, ClientLogin, SalesAccountLogin, RetentionAnalyst, Quote, \
                     ClientUser, ClientUpdate, ContractNumber])

admin.site.register(Client, SimpleHistoryAdmin)

class ContractUpdateEmailLogAdmin(admin.ModelAdmin):
    readonly_fields = ('time',)
admin.site.register(ContractUpdateEmailLog,ContractUpdateEmailLogAdmin)
