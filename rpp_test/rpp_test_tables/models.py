from django.db import models
from datetime import datetime, timedelta, timezone
from django.utils import timezone
import pytz
from simple_history.models import HistoricalRecords

class SalesAccount(models.Model):
    user = models.OneToOneField('auth.User')
    name = models.CharField(max_length=144)
    retention_analyst = models.ForeignKey('RetentionAnalyst')
    vendors = models.ManyToManyField('Vendor')
    email = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    @property
    def recent_sales(self):
        return [x for x in self.contract_set.all() if x.start_date > timezone.now().date() + timedelta(days=7)]

class Client(models.Model):
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=30)
    status = models.BooleanField(default=True)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    zip_code = models.CharField(max_length=100)
    website = models.CharField(max_length=200)
    web_admin = models.CharField(max_length=100,null=True,blank=True)
    web_admin_cell = models.CharField(max_length=100,null=True,blank=True)
    correspondence_signature = models.CharField(max_length=100)
    correspondence_signature_title = models.CharField(max_length=100)
    correspondence_phone = models.CharField(max_length=100)
    correspondence_contact_one = models.CharField(max_length=100)
    correspondence_contact_two = models.CharField(max_length=100,null=True,blank=True)
    correspondence_email = models.CharField(max_length=100)
    copy_emails_to_portal_list = models.IntegerField(null=True,blank=True)
    list_discount = models.CharField(max_length=100,null=True,blank=True)
    list_discount_international = models.CharField(max_length=100,null=True,blank=True)
    service_discount = models.CharField(max_length=100,null=True,blank=True)
    override_sales_tax = models.FloatField()
    discount_off_list_percent = models.FloatField()
    rate_factor_year = models.FloatField()
    rate_factor_two_year = models.FloatField()
    rate_factor_three_year = models.FloatField()
    residual_value = models.FloatField()
    aquisition_fee = models.FloatField()
    rate_factor_increase = models.FloatField(null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    last_changed_by = models.CharField(max_length=100,null=True,blank=True)
    email_address_salesperson = models.CharField(max_length=100,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey('auth.User')
    history = HistoricalRecords()

    def __str__(self):
        return self.name

    @property
    def active_sales_agents(self):
        a = []
        for con in Contract.objects.filter(client=self):
            if con.sale_agent not in a:
                a.append(con.sale_agent)
        return a

    def active_analysts(self):
        a = []
        for agent in self.active_sales_agents:
            if agent.retention_analyst not in a:
                a.append(agent.retention_analyst)
        return a

class Vendor(models.Model):
    name = models.CharField(max_length=200)
    lead_product = models.CharField(max_length=200)
    phone = models.CharField(max_length=30)
    status = models.BooleanField(default=True)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    zip_code = models.CharField(max_length=100)
    website = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Manufacturer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    website = models.CharField(max_length=200)
    phone = models.CharField(max_length=40)
    status = models.BooleanField(default=True)
    logo = models.FileField(null=True,blank=True)

    def __str__(self):
        return self.name

    @property
    def image_url(self):
        if self.logo:
            return self.logo.url
        return "http://images.clipartpanda.com/animated-question-mark-for-powerpoint-1256186461796715642question-mark-icon.svg.hi.png"

class PartNumber(models.Model):
    manufacturer = models.ForeignKey(Manufacturer)
    desc = models.TextField()
    end_of_service = models.DateField()
    end_of_life = models.DateField()
    end_of_support = models.DateField()
    trade_value = models.FloatField(null=True,blank=True)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.manufacturer.name +  ' ' + self.desc

class Product(models.Model):
    part_num = models.ForeignKey(PartNumber)
    serial = models.CharField(max_length=200)
    client = models.ForeignKey(Client)
    purchase_date = models.DateField()
    disposal = models.DateField(null=True,blank=True)
    service_category = models.CharField(null=True,blank=True,max_length=100)
    price = models.FloatField()
    quantity_min = models.CharField(null=True,blank=True,max_length=100)
    quantity_max = models.CharField(null=True,blank=True,max_length=100)
    duration = models.CharField(null=True,blank=True,max_length=100)
    orderability = models.CharField(null=True,blank=True,max_length=100)
    item_identifier = models.CharField(null=True,blank=True,max_length=100)
    service_program = models.CharField(max_length=100)
    category_base_discount_name = models.CharField(null=True,blank=True,max_length=100)
    end_of_sale_date = models.DateField(null=True,blank=True)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.client.name + ' ' + self.part_num.desc
    @property
    def ninety(self):
        contracts = self.contract_set.all()
        for con in contracts:
            if timezone.now().date() + timedelta(days=90) > con.end_date:
                return True
        return False

    def not_trued(self):
        contracts = self.contract_set.all()
        if contracts.count() <= 1:
            return False
        end = contracts.first().end_date
        for con in contracts:
            if con.end_date != end:
                return True
        return False

    def serv_vendor(self):
        con = self.contract_set.all().first()
        return con.vendor

    def serv_end(self):
        x = self.contract_set.all().first().end_date
        for con in self.contract_set.all():
            if con.end_date < x:
                x = con.end_date
        return x

    def has_contract(self):
        if self.contract_set.all():
            return True
        return False

    def sales_agent(self):
        return self.sales_agents()[0]

    def sales_agents(self):
        return [cont.sale_agent for cont in self.contract_set.all()]

    def ret_analyst(self):
        return [agent.retention_analyst for agent in self.sales_agents()]


class Contract(models.Model):
    client = models.ForeignKey(Client)
    product = models.ForeignKey(Product)
    contract_number = models.ForeignKey('ContractNumber')
    serial = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    cost = models.FloatField()
    vendor = models.ForeignKey(Vendor)
    sale_agent = models.ForeignKey(SalesAccount)
    commission = models.FloatField(default=0)
    service_level = models.CharField(max_length=200)
    site_id = models.CharField(max_length=200)
    site_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    zip_code = models.CharField(max_length=200)
    country = models.CharField(max_length=100)
    quantity = models.CharField(max_length=200)
    pak_num = models.CharField(max_length=200)
    smart_net_sku = models.CharField(max_length=200,null=True,blank=True)
    smark_net_desc = models.CharField(max_length=200,null=True,blank=True)
    annual_list_price = models.CharField(max_length=200,null=True,blank=True)
    trade_value = models.CharField(max_length=200,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.client.name + self.vendor.name + str(self.cost)

    @property
    def days_left(self):
        end = self.end_date
        td =  self.end_date - timezone.now().date()
        return td.days

    def ninety(self):
        if timezone.now().date() + timedelta(days=180) > self.end_date:
            return True
        return False

    def active(self):
        if self.end_date > timezone.now().date():
            return True
        return False

    def end_of_life(self):
        return self.product.part_num.end_of_life

    def end_of_service(self):
        return self.product.part_num.end_of_service


class ClientLogin(models.Model):
    client = models.ForeignKey(Client)
    time = models.DateTimeField(auto_now_add=True)

class SalesAccountLogin(models.Model):
    agent = models.ForeignKey(SalesAccount)
    time = models.DateTimeField(auto_now_add=True)

class AdminLogin(models.Model):
    admin = models.ForeignKey('auth.user')
    time = models.DateTimeField(auto_now_add=True)

# class AdminChange(model.Model):
#     admin = models.ForeignKey('auth.user')
#     time = models.DateTimeField(auto_now_add=True)

class RetentionAnalyst(models.Model):
    name = models.CharField(blank=True,null=True,max_length=100)
    email = models.CharField(max_length=100)
    user = models.OneToOneField('auth.User')

    def __str__(self):
        return str(self.name)

    @property
    def contract_count(self):
        return len([x for x in Contract.objects.all() if x.sale_agent.retention_analyst == self])


class ClientUser(models.Model):
    user = models.OneToOneField('auth.user')
    client = models.ForeignKey(Client)
    cell = models.CharField(max_length=100)

    def __str__(self):
        return self.user.username + ' ' + self.client.name

class Quote(models.Model):
    client = models.ForeignKey(Client,null=True,blank=True)
    sales_agent = models.ForeignKey(SalesAccount,null=True,blank=True)
    contract = models.ForeignKey(Contract,null=True,blank=True)
    service_level = models.CharField(max_length=20)
    service_level_desc = models.CharField(max_length=100)
    vendor = models.ForeignKey(Vendor)
    date = models.DateField(auto_now_add=True)
    quantity = models.IntegerField()
    install_site_name = models.CharField(max_length=100)
    install_site_id = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    begin_date = models.DateField()
    end_date = models.DateField()
    smartnet_service_sku = models.CharField(max_length=100)
    list_price = models.FloatField()
    unit_price = models.FloatField()
    extended_price = models.FloatField()

class ClientUpdate(models.Model):
    user = models.ForeignKey('auth.user')
    client = models.ForeignKey(Client)
    time = models.DateTimeField(auto_now_add=True)

class ContractUpdate(models.Model):
    user = models.ForeignKey('auth.User')
    contract = models.ForeignKey(Contract)
    time = models.DateTimeField(auto_now_add=True)

class ContractUpdateEmailLog(models.Model):
    contract = models.ForeignKey(Contract)
    recipient = models.CharField(max_length=100)
    time = models.DateTimeField(auto_now_add=True)
    details = models.TextField()

class ContractReportEmailLog(models.Model):
    contract = models.ForeignKey('ContractNumber')
    recipient = models.ForeignKey(Client)
    time = models.DateTimeField(auto_now_add=True)
    chart = models.FileField()
    completed = models.BooleanField(default=False)

    @property
    def chart_path(self):
        return 'excel/{}-{}-{}.xlsx'.format(self.contract.client.name, self.contract.number,self.time.strftime('%m-%d-%y'))

class ContractNumber(models.Model):
    number = models.CharField(max_length=100)
    client = models.ForeignKey(Client)
    vendor = models.ForeignKey(Vendor)
    sales_agent = models.ForeignKey(SalesAccount)

    @property

    def closest_end_date(self):
        x = self.contract_set.all().first().end_date
        for e in self.contract_set.all():
            if e.end_date < x:
                x = e.end_date
        return x

    def closest_end_life(self):
        x = self.contract_set.all().first().product.part_num.end_of_life
        for e in self.contract_set.all():
            if e.product.part_num.end_of_life < x:
                x = e.product.part_num.end_of_life
        return x

    def closest_end_of_service(self):
        x = self.contract_set.all().first().product.part_num.end_of_service
        for e in self.contract_set.all():
            if e.product.part_num.end_of_service < x:
                x = e.product.part_num.end_of_service
        return x

    def closest_end_of_support(self):
        x = self.contract_set.all().first().product.part_num.end_of_support
        for e in self.contract_set.all():
            if e.product.part_num.end_of_support < x:
                x = e.product.part_num.end_of_support
        return x

    def trued(self):
        x = self.contract_set.all().first().end_date
        for a in self.contract_set.all():
            if a.end_date != x:
                return False
        return True

    def end_date_half(self):
        if self.closest_end_date < datetime.now().date() + timedelta(days=180):
            return True
        return False

    def end_date_quarter(self):
        if self.closest_end_date < datetime.now().date() + timedelta(days=90):
            return True
        return False
