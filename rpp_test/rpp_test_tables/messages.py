from datetime import datetime, timedelta, timezone
from django.utils import timezone
from rpp_test_tables.models import Client, Contract, Product

def get_new_clients():
    td = timedelta(days=1)
    return Client.objects.filter(created_at__gt=timezone.now() - td)

con = Contract.objects.all()
prod = Product.objects.all()

def get_trueups():
    xs = []
    for product in prod:
        if product.contract_set.all().count() > 1:
            end = product.contract_set.all().first().end_date
            for con in product.contract_set.all():
                if con.end_date != end:
                    xs.append(con)
                    xs.append(product.contract_set.all().first())
    return xs

def get_ending_contracts():
    ls = []
    for x in con:
        if x.end_date < timezone.now().date() + timedelta(days=180) and x.end_date > timezone.now().date():
            ls.append(x)
    return ls

def get_new_contracts():
    td = timedelta(days=1)
    return Contract.objects.filter(created_at__gt=timezone.now() - td)


def new_client_message_list():
    messages = []
    for client in get_new_clients():
        messages.append([client.name, client.created_at.strftime('%D')])
    return messages
