from django.shortcuts import render
from rpp_test_tables.models import Manufacturer
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy

class ManufacturerCreateView(CreateView):
    model = Manufacturer
    fields = ['name', 'address', 'website', 'phone', 'status', 'logo']
    def form_valid(self, form):
        instance = form.save(commit=False)
        return super().form_valid(form)
    def get_success_url(self):
        return reverse('manufacturer_list_view')

class ManufacturerUpdateView(UpdateView):
    model = Manufacturer
    fields = ['name', 'address', 'website', 'phone', 'status', 'logo']
    def get_success_url(self):
        return reverse('manufacturer_list_view')

class ManufacturerDeleteView(DeleteView):
    model = Manufacturer
    def get_success_url(self):
        return reverse('manufacturer_list_view')
