from django.shortcuts import render
from rpp_test_tables.models import RetentionAnalyst
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy

class RetentionAnalystDetailView(DetailView):
    model = RetentionAnalyst

class RetentionAnalystCreateView(CreateView):
    model = RetentionAnalyst
    fields = []
    def form_valid(self, form):
        instance = form.save(commit=False)
        return super().form_valid(form)
    def get_success_url(self):
        return reverse('retention_analyst_list_view')

class RetentionAnalystUpdateView(UpdateView):
    model = RetentionAnalyst
    fields = []
    def get_success_url(self):
        return reverse('retention_analyst_list_view')

class RetentionAnalystDeleteView(DeleteView):
    model = RetentionAnalyst
    def get_success_url(self):
        return reverse('retention_analyst_list_view')
