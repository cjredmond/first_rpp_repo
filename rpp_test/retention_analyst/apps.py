from django.apps import AppConfig


class RetentionAnalystConfig(AppConfig):
    name = 'retention_analyst'
