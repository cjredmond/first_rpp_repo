from django.shortcuts import render
from rpp_test_tables.models import SalesAccount, Contract, Client
from django.views.generic import DetailView, TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from datetime import datetime, timedelta, timezone
from django.utils import timezone

class SalesAccountCreateView(CreateView):
    model = SalesAccount
    fields = ['name', 'retention_analyst', 'vendors']
    def get_success_url(self):
        return reverse('salesaccount_list_view')
    def form_valid(self,form):
        instace = form.save(commit=False)
        return super().form_valid(form)

class SalesAccountUpdateView(UpdateView):
    model = SalesAccount
    fields = ['name', 'vendors', 'retention_analyst']
    def get_success_url(self,**kwargs):
        return reverse('salesaccount_detail_view', args=self.kwargs['pk'] )

class SalesAccountDeleteView(DeleteView):
    model = SalesAccount
    def get_success_url(self):
        return reverse('sales_account_list_view', 'number')

class SalesDetailView(DetailView):
    model = SalesAccount
    def get_context_data(self,**kwargs):
        context = super().get_context_data()
        agent = SalesAccount.objects.get(id=self.kwargs['pk'])
        sales_user = SalesUser.objects.get(user=self.request.user)
        context['con_added_week'] = Contract.objects.filter(sale_agent=agent,end_date__gt=timezone.now().date() - timedelta(days=7))
        context['con_added_month'] = Contract.objects.filter(sale_agent=agent,end_date__gt=timezone.now().date() - timedelta(days=30))
        context['commission_week'] = sum([con.commission for con in Contract.objects.filter(sale_agent=agent,end_date__gt=timezone.now().date() - timedelta(days=7))])
        context['commission_month'] = sum([con.commission for con in Contract.objects.filter(sale_agent=agent,end_date__gt=timezone.now().date() - timedelta(days=30))])
        context['clients_added_week'] = sales_user.clients_added.filter(created_at__gt=timezone.now().date() - timedelta(days=7))
        context['clients_added_month'] = sales_user.clients_added.filter(created_at__gt=timezone.now().date() - timedelta(days=30))
        return context

class SalesContractListView(ListView):
    model = Contract
    def get_queryset(self,**kwargs):
        if self.kwargs['category'] == 'number':
            return Contract.objects.filter(sale_agent=SalesAccount.objects.filter(id=self.kwargs['pk'])).filter(sale_agent_id=self.request.user.salesuser.sales_account.id).order_by('serial')
        elif self.kwargs['category'] == 'client':
            return Contract.objects.filter(sale_agent=SalesAccount.objects.filter(id=self.kwargs['pk'])).filter(sale_agent_id=self.request.user.salesuser.sales_account.id).order_by('client')
        elif self.kwargs['category'] == 'vendor':
            return Contract.objects.filter(sale_agent=SalesAccount.objects.filter(id=self.kwargs['pk'])).filter(sale_agent_id=self.request.user.salesuser.sales_account.id).order_by('vendor')
        elif self.kwargs['category'] == 'commission':
            return Contract.objects.filter(sale_agent=SalesAccount.objects.filter(id=self.kwargs['pk'])).filter(sale_agent_id=self.request.user.salesuser.sales_account.id).order_by('-commission')
        else:
            return Contract.objects.filter(sale_agent=SalesAccount.objects.filter(id=self.kwargs['pk'])).filter(sale_agent_id=self.request.user.salesuser.sales_account.id).order_by('end_date')

class SalesClientListView(TemplateView):
    template_name = 'sales_client_list.html'
    def get_context_data(self,**kwargs):
        context = super().get_context_data()
        agent = SalesAccount.objects.get(id=self.kwargs['pk'])
        ls = []
        for client in Client.objects.all():
            if agent in client.active_sales_agents:
                ls.append(client)
        context['clients'] = ls
        return context
